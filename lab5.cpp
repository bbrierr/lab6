#include <iostream>
#include <iomanip>
#include <algorithm>
#include <sstream>
#include <string>
#include "stdlib.h"
#include "string.h"
using namespace std;
enum Suit { SPADES=0, HEARTS=1, DIAMONDS=2, CLUBS=3 };

//I added this comment for the lab6 step 4 
//I added this to fix the git repository
typedef struct Card {
  Suit suit;
  int value;
} Card;

string get_suit_code(Card& c);
string get_card_name(Card& c);
bool suit_order(const Card& lhs, const Card& rhs);
int myrandom (int i) { return std::rand()%i;}


int main(int argc, char const *argv[]) {
  // IMPLEMENT as instructed below
  /*This is to seed the random generator */
  srand(unsigned (time(0)));


//  Card card;
  int b = 0;
  Card cardDeck[52];

  for(int i = 0; i < 4; i++){
    for (int val = 2; val < 15; val++){

//        Card card{suit = static_cast<Suit>(i),value = j};
        //card = card.suit + card.value;

        cardDeck[b].suit = (enum Suit)i;
        cardDeck[b].value = val;
        b++;
    }
  }
  /*Create a deck of cards of size 52 (hint this should be an array) and
   *initialize the deck*/


   random_shuffle(cardDeck, cardDeck + 52, myrandom);
  /*After the deck is created and initialzed we call random_shuffle() see the
   *notes to determine the parameters to pass in.*/
Card myDeck[5];

myDeck[0] = cardDeck[0];
myDeck[1] = cardDeck[1];
myDeck[2] = cardDeck[2];
myDeck[3] = cardDeck[3];
myDeck[4] = cardDeck[4];





/*for (int a = 0; a < 5; a++){
myDeck[a] = cardDeck[a];
}*/


   /*Build a hand of 5 cards from the first five cards of the deck created
    *above*/


    sort(myDeck, myDeck + 5, suit_order);
    cout << endl;
    for(int x = 0; x < 5; x++){
      cout << get_card_name(myDeck[x]) << " " << get_suit_code(myDeck[x]) << endl;
    }
    /*Sort the cards.  Links to how to call this function is in the specs
     *provided*/

    /*Now print the hand below. You will use the functions get_card_name and
     *get_suit_code */
  return 0;
}


/*This function will be passed to the sort funtion. Hints on how to implement
* this is in the specifications document.*/
bool suit_order(const Card& lhs, const Card& rhs) {
  // IMPLEMENT
  //not done yet
  bool b = false;

  if (lhs.suit < rhs.suit){
    b = true;
  }
  if( lhs.suit == rhs.suit){
    if(lhs.value < rhs.value){
      b = true;
    }
  }
  return b;
}

string get_suit_code(Card& c) {
  switch (c.suit) {
    case SPADES:    return "\u2660";
    case HEARTS:    return "\u2661";
    case DIAMONDS:  return "\u2662";
    case CLUBS:     return "\u2663";
    default:        return "";
  }
}

string get_card_name(Card& c) {
  switch (c.value){
    case 11:  return "Jack of ";
    case 12:  return "Queen of ";
    case 13:  return "King of ";
    case 14:  return "Ace of ";
    default:  return to_string(c.value) + " of ";
  }
}
